﻿param (
    [Parameter(Mandatory=$true)]
    [string]$ScriptPath
)

 # Get the script line-by-line for Tokenization in C#:
 $scriptData = Get-Content $ScriptPath

 $astData = [System.Management.Automation.Language.Parser]::ParseFile($ScriptPath, [ref]$null, [ref]$null)

 # Create the C# object passing the script content parameter:
 #$PSInterface = New-Object -TypeName "Apprenda.PSParamMenu" -ArgumentList $scriptData

 $outResultMain = $astData | Out-File -FilePath "C:\WorkingFolder\pswui\astformat.txt"
 $outResultParam = $astData.ParamBlock | Out-File -FilePath "C:\WorkingFolder\pswui\astParam.txt"
 $outResultEnd = $astData.EndBlock | Out-File -FilePath "C:\WorkingFolder\pswui\astEnd.txt"
 $outResultExtent = $astData.Extent | Out-File -FilePath "C:\WorkingFolder\pswui\astExtent.txt"
 $outResultOther = $astData.GetScriptBlock() | Out-File -FilePath "C:\WorkingFolder\pswui\astOther.txt"
 $outResultParamAtt = $astData.ParamBlock.Parameters | Out-File -FilePath "C:\WorkingFolder\pswui\astParamAtts.txt"


# Show the UI:
$PSInterface.ShowDialog


$func1 = function helpme {
    param($teststring)

    Write-Host $teststring
}

$func1