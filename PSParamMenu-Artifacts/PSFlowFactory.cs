﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.PowerShell.EditorServices.Console;
using System.Collections.ObjectModel;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using Microsoft.PowerShell.EditorServices;

namespace PSParamMenu
{
    public class PSFlowFactory
    {

        public static void BuildWpf()
        {
            
        }

        public static void BuildWeb()
        {
            
        }

        public static PSWorkFlow CreatePSWorkFlowFromParams(dynamic paramsBlock)
        {
            // Create a temp workflow to build:
            var tempFlow = new PSWorkFlow();

            // Get each param from the block:
            foreach (var param in paramsBlock.Parameters)
            {
                // Add each param as a variable:
                tempFlow.AddVariable(param.Name, attributes: param.Attributes);
            }

            // Create PSTask to execute:
            var tempTask = new 

            return tempFlow;

        }

    }
}
