﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Threading;
using Microsoft.PowerShell.EditorServices;
using Microsoft.PowerShell.Commands;
using Microsoft.PowerShell;

namespace PSParamMenu
{
    public class PSTask
    {
        // Variable Declarations:
        private string _taskName;
        private PSCommand _taskCommand;
        private PSCommand _taskRestore;

        public PSTask()
        {
            
        }

        public PSTask(string name, PSCommand script, PSCommand restore = null)
        {
            _taskCommand = script;
            _taskRestore = restore;
            var block = ScriptBlock.Create(script.ToString());
        }

        public string Name
        {
            get { return _taskName; }
            set { _taskName = value; }
        }

        public PSCommand ScriptCommand
        {
            get { return _taskCommand; }
            set { _taskCommand = value; }
        }

        public PSCommand RestoreCommand
        {
            get { return _taskRestore; }
            set { _taskRestore = value; }
        }



    }
}
