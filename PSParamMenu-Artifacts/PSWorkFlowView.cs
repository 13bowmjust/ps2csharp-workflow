﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto;
using Eto.Forms;
using Microsoft.PowerShell;
using System.Management.Automation;

namespace PSParamMenu
{
    public class PSWorkFlowView : INavigationItem
    {
        public Control Content { get; set; }

        public string Text { get; set; }

        public string Key { get; set; }

        private List<dynamic> _flowObjects;

        private Type _flowType;

        public PSWorkFlowView()
        {
        }

        public PSWorkFlowView(string text, string key, List<dynamic> flowObjects )
        {
            _flowObjects = flowObjects;
            Text = text;
            Key = key;
        }

        public void BuildControl()
        {
            // Get the Type of the controls:
            _flowType = _flowObjects[0].GetType();

            // Build control according to _flowType:
            PowerShell psMainObject = PowerShell.Create();


        }
    }
}
