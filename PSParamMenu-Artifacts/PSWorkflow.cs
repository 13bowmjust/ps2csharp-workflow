﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using Microsoft.PowerShell.EditorServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;
using Eto;
using System.IO;
using Microsoft.PowerShell.EditorServices.Console;

namespace PSParamMenu
{
    public class PSWorkFlow
    {

        // Declarations:

        private Queue<PSTask> _workflowTasks;
        private Dictionary<string, PSVariable> _workflowVars;
        private List<PSObject> _workflowErrors; 

        // Methods:

        public PSWorkFlow()
        {
            // Setup the Navigation Control:
            //this.Bind<List<WorkFlowView>>("WorkFlows", DirectBinding<List<WorkFlowView>>.Delegate<List<WorkFlowView>>(() => this.WorkFlows));

            // Initialize:
            _workflowTasks = new Queue<PSTask>();

        }

        public void AddTask(PSTask task)
        {
            // Queue the PSTask for execution:
            _workflowTasks.Enqueue(task);
        }

        public void AddTasks(PSTask[] tasks)
        {
            // Adds each task from 
            foreach (var task in tasks)
            {
                _workflowTasks.Enqueue(task);
            }
        }

        public PSTask RemoveTask()
        {
            // Dequeues and prepares the task for execution:
            return _workflowTasks.Dequeue();
        }

        public void CreateTask()
        {
            
        }

        public void Execute(PowerShell session)
        {
            // Execute each task for the Tasks queue:
            while (_workflowTasks.Count > 0)
            {
                // Dequeue the task and complete its execution cycle:
                var task = _workflowTasks.Dequeue();

                // Add the task command, parameter(s), etc:
                session.Commands = task.ScriptCommand;

                // Now that our current task is in the session, invoke:
                var errors = session.Invoke();

                // IF there are errors, log and run restore:
                if (errors.Count > 0)
                {
                    // Copy the errors, if any:
                    _workflowErrors.AddRange(errors);

                    // Run RestoreScript if it exists:
                    if (task.RestoreCommand != null)
                    {
                        // Execute the RestoreCommand script:
                        session.Commands = task.RestoreCommand;

                        // Add restore errors, if any:
                        _workflowErrors.AddRange(session.Invoke());
                    }
                }

            }
        }

        public void AddVariable(string name,
            dynamic value = null,
            ScopedItemOptions options = ScopedItemOptions.Unspecified,
            Collection<Attribute> attributes = null)
        {
            // Create a PSVariable for the workflow:
            var tempVar = new PSVariable(name, value, options, attributes);

            // Add the PSVariable to the collection:
            _workflowVars.Add(name, tempVar);
        }

        public PSVariable GetVariable(string name)
        {
            return _workflowVars[name];
        }

        public Queue<PSTask> Tasks
        {
            get { return _workflowTasks; }
            set { _workflowTasks = value; }
        } 

        //public PowerShell Session
        //{
        //    get { return _psSession; }
        //    set { _psSession = value; }
        //}

    }
}
