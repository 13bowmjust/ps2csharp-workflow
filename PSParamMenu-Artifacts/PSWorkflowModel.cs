﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.PowerShell.EditorServices.Console;
using System.Collections.ObjectModel;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using Microsoft.PowerShell.EditorServices;
using System.Management.Automation.Provider;

namespace PSParamMenu
{
    public class PSWorkflowModel
    {

        // Declarations:
        private List<PSWorkFlow> _workFlows;
        private PowerShell _psSession;

        // Methods:

        public PSWorkflowModel(string psPath, string psScript)
        {

            var fullPath = $"{ psPath }\\{ psScript }";

            _psSession = PowerShell.Create();

            var context = new PowerShellContext();
            context.SetWorkingDirectory(psPath);

            var host = new ConsoleService(context);

            var session = new EditorSession();
            session.StartSession();

            var scriptfile = new ScriptFile(fullPath,
                "clonedPSScript.ps1",
                new StreamReader(fullPath),
                session.PowerShellContext.PowerShellVersion);

            var tempScript = scriptfile.ScriptAst;

            // Begin creating the workflows for the selected script:

            // 1.) Create params tasks flow:
            

            //_workFlows.Add();

        }

        public void ShowDialog()
        {
            
        }

        public void Initialize(params object[] scriptParams)
        {

        }

    }
}
