﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Language;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Eto.Drawing;
using Eto.Forms;
using PSWUI.Client.Models;
using PSWUI.Client.Views;
using PSWUI.Shared.WCF.PSFlow;

namespace PSWUI.Client
{
    public static class ClientFlowFactory
    {

        public static TableLayout CreateLayout(WorkFlowModel model = null, PSWorkFlow flow = null)
        {
            var rows = new List<TableRow>();

            if (model != null)
            {
                flow = model.CurrentWorkFlow;
            }

            // Create the Title Row:
            rows.Add(CreateTitleRow(flow));

            // Create rows for tasks:
            foreach (var task in flow.Tasks)
            {
                if (flow.TaskType == TaskType.Input)
                {
                    rows.Add(CreateRowFromTask(task, flow.TaskType, flow.GetVariable(task.Name)));
                }
                else
                {
                    rows.Add(CreateRowFromTask(task, flow.TaskType));
                }
            }

            rows.Add(CreateNavigationRow());
            rows.Add(new TableRow());

            var layout = new TableLayout(rows)
            {
                Spacing = new Size(10, 10),
                Padding = new Padding(10, 10, 10, 10)
            };

            return layout;

        }

        public static TableRow CreateTitleRow(PSWorkFlow flow)
        {
            var row = new TableRow();

            var title = new Label();
            title.TextBinding.Bind(flow, r => $"{flow.Tasks.Peek().ScriptCommand.Ast.Extent.File} - {r.TaskType.ToString()}");
            title.Font = new Font("Segoe UI", 18.0f);

            var cell = new TableCell(title);

            row = new TableRow(cell);

            return row;
        }

        public static TableRow CreateRowFromTask(PSTask task, TaskType type, PSVariable variable = null)
        {

            var cells = new List<TableCell>();

            // Add new cell for ID:
            var lblID = new Label();
            lblID.TextBinding.Bind(task, r => r.Name);
            cells.Add(new TableCell(lblID));

            switch (type)
            {

                // Determine LOAD:
                case TaskType.Load:
                    {
                        // Build Action:
                        var lblAction = new Label();
                        lblAction.TextBinding.Bind(task, r => r.ScriptCommand.Ast.Extent.Text);
                        cells.Add(new TableCell(lblAction, true));

                        break;
                    }
                // Determine INPUT:
                case TaskType.Input:
                    {
                        if (variable != null)
                        {
                            // IF Variable is a string:
                            if (task.TaskType == typeof(string))
                            {
                                // Build Action:
                                task.Input = "";
                                var txtAction = new TextBox();
                                txtAction.TextBinding.Bind(task, r => (string)r.Input);

                                cells.Add(new TableCell());
                                cells.Add(new TableCell(txtAction, true));
                            }
                            // IF Variable is a bool:
                            else if (task.TaskType == typeof(SwitchParameter))
                            {
                                // Build Action:
                                task.Input = new SwitchParameter(false);
                                var chkAction = new CheckBox();
                                chkAction.Text = "Enable?";
                                chkAction.CheckedBinding.Bind(task, r => ((SwitchParameter)r.Input).IsPresent);

                                cells.Add(new TableCell());
                                cells.Add(new TableCell(chkAction, true));
                            }
                            else if (task.TaskType == typeof(PSCredential))
                            {
                                task.Input = new PSCredential("Enter User:", new SecureString());
                                var lbl1 = new Label();
                                lbl1.Text = "U: ";
                                var txt1 = new TextBox();
                                txt1.TextBinding.Bind(task, r => ((PSCredential)r.Input).UserName);
                                var lbl2 = new Label();
                                lbl2.Text = "P: ";
                                var txt2 = new PasswordBox();
                                txt2.TextChanged += (sender, args) =>
                                {
                                    if (txt2.DataContext != null)
                                    {
                                        var user = ((PSCredential)task.Input).UserName;
                                        var secure = new SecureString();

                                        ((PasswordBox)sender).Text.ToList().ForEach(c => secure.AppendChar(c));

                                        task.Input = new PSCredential(user, secure);
                                    }
                                };

                                cells.Add(new TableCell(lbl1));
                                cells.Add(new TableCell(txt1, true));
                                cells.Add(new TableCell(lbl2));
                                cells.Add(new TableCell(txt2, true));

                            }
                        }

                        break;
                    }

                // Determine OUTPUT:
                case TaskType.Output:
                    {
                        // Build Action:
                        var lblOutput = new Label();
                        lblOutput.Wrap = WrapMode.Word;
                        lblOutput.TextBinding.Bind(task, r => r.Output);

                        cells.Add(new TableCell(lblOutput, true));

                        break;
                    }

            }

            return new TableRow(cells);
        }

        public static TableRow CreateNavigationRow()
        {
            var cells = new List<TableCell>();

            var btnCancel = new Button();
            btnCancel.BindDataContext(btn => btn.Command, (WorkFlowModel w) => w.CancelFlow);
            btnCancel.Text = "Cancel";
            btnCancel.Width = 64;
            btnCancel.Height = 28;
            btnCancel.Size = new Size(64, 28);

            var btnNext = new Button();
            btnNext.BindDataContext(btn => btn.Command, (WorkFlowModel w) => w.NextFlow);
            btnNext.Text = "Next";
            btnNext.Width = 64;
            btnNext.Height = 28;
            btnNext.Size = new Size(64, 28);

            cells.Add(new TableCell(btnCancel));
            cells.Add(new TableCell());
            cells.Add(new TableCell(btnNext));

            var row = new TableRow(cells)
            {
                ScaleHeight = false,
            };

            return row;
        }

    }
}
