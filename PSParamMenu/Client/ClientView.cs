﻿using System;
using System.ServiceModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xaml;
using Eto;
using Eto.Forms;
using PSWUI.Shared.WCF.PSFlow;
using System.ServiceModel.Description;
using Eto.Drawing;
using PSWUI.Client.Models;
using PSWUI.Client.Views;
using PSWUI.Shared.WCF;

namespace PSWUI.Client
{
    public class ClientView : Form
    {

        public ClientView(string scriptPath)
        {
            if (string.IsNullOrEmpty(scriptPath))
            {
                var ofd = new OpenFileDialog();
                ofd.ShowDialog(this.Parent);

                scriptPath = ofd.FileName;
            }

            this.Title = "PSService";
            this.ClientSize = new Size(-1, -1);
            //this.Size = new Size(-1, -1);
            this.WindowStyle = WindowStyle.Default;
            var wfm = new WorkFlowModel(scriptPath);
            this.DataContext = wfm;

            this.BindDataContext(con => con.Content, (WorkFlowModel m) => m.Layout);
        }
    }
}
