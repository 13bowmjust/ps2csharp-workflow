﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;
using PSWUI.Shared.WCF;
using PSWUI.Shared.WCF.PSFlow;

namespace PSWUI.Client.Models
{
    public class WorkFlowModel : INotifyPropertyChanged
    {
        public PSService Service { get; set; }
        public RelayCommand<string> NextFlow { get; set; }
        public RelayCommand<string> CancelFlow { get; set; }
        public ServiceHost Host { get; set; }

        private List<TableLayout> _layout;
        private TableLayout _layoutCurrent;

        public WorkFlowModel(string scriptPath)
        {
            NextFlow = new RelayCommand<string>(Next);
            CancelFlow = new RelayCommand<string>(Cancel);

            _layout = new List<TableLayout>();

            Service = new PSService(scriptPath);

            Host = new ServiceHost(Service, new Uri("http://localhost:8081/PSService"));
            
            var smb = new ServiceMetadataBehavior();
            smb.HttpGetEnabled = true;
            smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
            Host.Description.Behaviors.Add(smb);

            Host.Open();

            foreach (var flow in Service.WorkFlows())
            {
                _layout.Add(ClientFlowFactory.CreateLayout(flow: flow));
            }

            _layoutCurrent = _layout[ViewIndex];

        }

        public void Next(string flowPos = "")
        {
            // Execute Commands:
            //Service.Execute();

            // Indicate workflow complete:
            if (ViewIndex < ViewWorkFlows.Count)
            {
                ViewIndex++;
                Layout = _layout[ViewIndex];
            }
        }

        public void Cancel(string str)
        {
            Host.Close();
            Application.Instance.Quit();
        }

        public PSWorkFlow CurrentWorkFlow
        {
            get { return Service.Model.CurrentFlow; }
        }

        public List<PSWorkFlow> ViewWorkFlows
        {
            get { return Service.WorkFlows(); }
        }

        public int ViewIndex
        {
            get { return Service.Model.CurrentIndex; }
            set
            {
                if (Service.Model.CurrentIndex != value)
                {
                    Service.Model.CurrentIndex = value;
                    PropChanged();
                }
            }
        }

        public TableLayout Layout
        {
            get { return _layoutCurrent; }
            set
            {
                if (_layoutCurrent != value)
                {
                    _layoutCurrent = value;
                    PropChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void PropChanged([CallerMemberName] string membername = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(membername));
        }
    }
}
