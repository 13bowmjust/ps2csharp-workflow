﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Text;
using System.Threading.Tasks;

using Eto;
using Eto.Forms;
using PSWUI.Client.Models;
using PSWUI.Shared.WCF.PSFlow;
using PSWUI.Client;

namespace PSWUI.Client.Views
{
    public class FormView : TableLayout
    {

        public FormView(PSWorkFlow formFlow, TableLayout layout) : base(layout.Rows)
        {
            // Data Items needed in the starting view:
            this.DataContext = formFlow;

            foreach (var row in layout.Rows)
            {
                this.Rows.Add(row);
            }

        }

        public FormView(FormView build) : base(build.Rows)
        {
            // Creates a FormView from a pre-built view:
        }

    }
}
