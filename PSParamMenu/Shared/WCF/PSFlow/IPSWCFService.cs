﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace PSWUI.Shared.WCF.PSFlow
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPSWCFService" in both code and config file together.
    [ServiceContract]
    public interface IPSWCFService
    {
        [OperationContract]
        void Execute();

        [OperationContract]
        List<PSWorkFlow> WorkFlows();

        [OperationContract]
        List<string> Logs();
    }
}
