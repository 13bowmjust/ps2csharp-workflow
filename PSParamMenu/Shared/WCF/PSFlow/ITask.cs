﻿using System.Management.Automation;
using System;
using System.ServiceModel;
using System.Text;
using System.Linq;
using System.Collections.Generic;

namespace PSWUI.Shared.WCF.PSFlow
{
    [ServiceContract]
    public interface ITask
    {
        string Name { get; set; }
        ScriptBlock ScriptCommand { get; set; }
        ScriptBlock RestoreCommand { get; set; }

        [OperationContract]
        PowerShell RunInSession(PowerShell session);

    }
}