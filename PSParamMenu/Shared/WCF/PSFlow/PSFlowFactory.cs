﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Management.Automation;
using Microsoft.PowerShell.EditorServices;
using System.Management.Automation.Language;

namespace PSWUI.Shared.WCF.PSFlow
{
    public class PSFlowFactory
    {

        public static PSWorkflowModel CreatePSWorkflowModel(string scriptpath)
        {
            // Begin creating the workflows for the selected script:
            var model = new PSWorkflowModel();

            // Load the script for the model:
            var script = LoadScriptFile(scriptpath, model.Session);
            model.AddScriptFile(script);

            if (PSWorkFlowHasFunctions(script.ScriptAst))
            {
                // 1.) Create Funtion Module:
                model.AddWorkFlow(PSFlowFactory.CreatePSWorkFlowModule(script.ScriptAst));
            }

            if (script.ScriptAst.ParamBlock != null)
            {
                // 2.) Create params tasks flow:
                model.AddWorkFlow(PSFlowFactory.CreatePSWorkFlowFromParams(script.ScriptAst.ParamBlock));
            }

            // IF the block has value, add it:
            if (script.ScriptAst.BeginBlock != null)
            {
                // 3.) Create Begin Block flow:
                model?.AddWorkFlow(PSFlowFactory.CreatePSWorkFlowFromBlock(script.ScriptAst.BeginBlock));
            }

            // IF the block has value, add it:
            if (script.ScriptAst.BeginBlock != null)
            {
                // 4.) Create Process Block flow:
                model?.AddWorkFlow(PSFlowFactory.CreatePSWorkFlowFromBlock(script.ScriptAst.ProcessBlock));
            }

            // 5.) Create End Block flow:
            model.AddWorkFlow(PSFlowFactory.CreatePSWorkFlowFromBlock(script.ScriptAst.EndBlock));

            // Return the created workflow model:
            return model;
        }

        public static ScriptFile LoadScriptFile(string path, PowerShell shell)
        {

            var script = new ScriptFile(path,
                "clonedPSScript.ps1",
                new StreamReader(path),
                shell.Runspace.Version);

            return script;
        }

        public static void BuildWpf()
        {
            
        }

        public static void BuildWeb()
        {
            
        }

        public static PowerShell CreateSession(PowerShell current, ScriptFile context)
        {
            return null;
        }

        public static ScriptBlock GetScriptBlockByType(ScriptFile context, 
            TokenKind kind,
            TokenFlags flags,
            EditorSession session)
        {
            // Find the tokens where the code exists:
            var contentFirst = context.ScriptTokens.First(token => (token.Kind == kind)
            && (token.TokenFlags == flags));

            var contentSecond = context.ScriptTokens
                .Where(token => token.Extent.StartLineNumber 
                > contentFirst.Extent.StartLineNumber);

            // Get the start and end position of the token metadata:
            var scriptStart = contentFirst.Extent.StartScriptPosition;
            var scriptEnd = contentFirst.Extent.EndScriptPosition;

            // Create a new BufferPosition for start and end of token metadata:
            var buffStart = new BufferPosition(scriptStart.LineNumber,
                scriptStart.ColumnNumber);
            var buffEnd = new BufferPosition(scriptEnd.LineNumber,
                scriptEnd.ColumnNumber);

            // Create a new BufferRange from start and end to get script commands:
            var range = new BufferRange(buffStart, buffEnd);

            // Gets the Script Commands within the specified range:
            var tempScript = context.ScriptAst.ParamBlock.Extent.Text;



            // Join the array of commands into one line:
            //var tempCommand = string.Join("", tempScript.ToArray()); 

            // Create a script block from the one line tempCommand:
            var tempBlock = ScriptBlock.Create(tempScript);

            // Return the ScriptBlock:
            return tempBlock;
        }

        public static ScriptBlock GetCommandByIndex(ScriptFile content, int index)
        {
            return ScriptBlock.Create(content.ScriptAst.EndBlock.Statements[index].Extent.Text);
        }

        public static bool PSWorkFlowHasFunctions(ScriptBlockAst astModule)
        {
            var scriptFunctions = astModule.FindAll(ast => ast.GetType() == typeof(FunctionDefinitionAst), true).Cast<FunctionDefinitionAst>();

            if (scriptFunctions.Any())
            {
                return true;
            }
            return false;
        }

        public static PSWorkFlow CreatePSWorkFlowModule(ScriptBlockAst astModule)
        {
            // Get all Function definitions from the AST:
            var scriptFunctions = astModule.FindAll(ast => ast.GetType() == typeof(FunctionDefinitionAst), true).Cast<FunctionDefinitionAst>();
            
            // Create a PSWorkFlow for the Functions to be imported:
            var tempFlow = new PSWorkFlow(type: TaskType.Load);

            foreach (var task in scriptFunctions)
            {
                // Create a PSTask for the module to be executed:
                var tempTask = new PSTask(task.Name, 
                    ScriptBlock.Create(task.Extent.Text));

                // Add the PSTask to the PSWorkFlow:
                tempFlow.AddTask(tempTask);
            }

            return tempFlow;

        }

        public static PSWorkFlow CreatePSWorkFlowFromBlock(NamedBlockAst astBlock)
        { 
            // IF there is a valid BeginBlock, create workflow:
            if (astBlock != null)
            {
                var tempFlow = new PSWorkFlow(type: TaskType.Output);

                foreach (var statement in astBlock.Statements)
                {
                    // Create a PSTask for the module to be executed:
                    var tempTask = new PSTask(astBlock.BlockKind.ToString(),
                        ScriptBlock.Create(statement.Extent.Text));

                    // Add the PSTask to the PSWorkFlow:
                    tempFlow.AddTask(tempTask);
                }

                return tempFlow;
            }
            else
            {
                return null;
            }

        }

        public static PSWorkFlow CreatePSWorkFlowFromParams(ParamBlockAst paramsBlock)
        {
            // Create a temp workflow to build:
            var tempFlow = new PSWorkFlow(type: TaskType.Input);

            // Get each param from the block:
            foreach (var param in paramsBlock.Parameters)
            {
                // Get Names from the var name:
                string fName = param.Name.VariablePath.UserPath;
                string vName = param.Name.Extent.Text;

                // Add each param as a variable:
                tempFlow.AddVariable(param);

                // Create PSTask to execute:
                var tempTask = new PSTask(fName,
                    ScriptBlock.Create($"New-Variable -Name {fName}"), type: param.StaticType);

                tempFlow.AddTask(tempTask);
            }

            return tempFlow;
        }

        public static bool PSWorkFlowHasParams(ParamBlockAst paramBlock)
        {
            if (paramBlock.Parameters.Any())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static dynamic LogException(Exception ex)
        {
            return ($"{ex.Message} >>>> { ex.InnerException } >>>> { ex.Source } >>>> { ex.StackTrace } >>>> { ex }");
        }

    }
}
