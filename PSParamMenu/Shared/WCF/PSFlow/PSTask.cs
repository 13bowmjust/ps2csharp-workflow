﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Runtime.Serialization;
using System.Threading;
using Microsoft.PowerShell.EditorServices;
using Microsoft.PowerShell.Commands;
using Microsoft.PowerShell;
using System.ServiceModel;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace PSWUI.Shared.WCF.PSFlow
{
    [DataContract]
    public class PSTask
    {
        // Variable Declarations:
        private string _taskName;
        private ScriptBlock _taskCommand;
        private ScriptBlock _taskRestore;
        private Collection<PSObject> _taskOutput;
        private object _taskInput;
        private Type _taskType;

        public PSTask()
        {

        }

        public PSTask(string name, ScriptBlock script, ScriptBlock restore = null, Type type = null)
        {
            _taskName = name;
            _taskCommand = script;
            _taskRestore = restore;
            _taskInput = "";
            _taskOutput = new Collection<PSObject>();
            _taskType = type;
        }

        public PowerShell RunInSession(PowerShell session)
        {
            session.AddScript(_taskCommand.Ast.Extent.Text);
            return session;
        }

        public void AddOutput(Collection<PSObject> output)
        {
            _taskOutput = output;
        }

        [DataMember]
        public string Name
        {
            get { return _taskName; }
            set { _taskName = value; }
        }

        [DataMember]
        public ScriptBlock ScriptCommand
        {
            get { return _taskCommand; }
            set { _taskCommand = value; }
        }

        [DataMember]
        public ScriptBlock RestoreCommand
        {
            get { return _taskRestore; }
            set { _taskRestore = value; }
        }

        [DataMember]
        public string Output
        {
            get { return String.Join(" | ", _taskOutput.Select(x => x.ImmediateBaseObject.ToString())); }
        }

        [DataMember]
        public object Input 
        {
            get { return _taskInput; }
            set { _taskInput = value; }
        }

        [DataMember]
        public Type TaskType
        {
            get { return _taskType; }
            set { _taskType = value; }
        }



    }
}
