﻿using System;
using System.ServiceModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Language;
using System.Runtime.Serialization;
using ICSharpCode.NRefactory6.CSharp;
using Microsoft.Build.Tasks.Xaml;

namespace PSWUI.Shared.WCF.PSFlow
{
    public enum TaskType { Load, Input, Output }

    [DataContract]
    public class PSWorkFlow
    {

        // Declarations:

        private Queue<PSTask> _workflowTasks;
        private Dictionary<string, PSVariable> _workflowVars;
        private List<dynamic> _workflowErrors;
        private bool _workflowAutoRestore;
        private TaskType _workflowViewType;

        // Methods:

        public PSWorkFlow(bool restore = false, TaskType type = TaskType.Output)
        {
            // Setup the Navigation Control:


            // Initialize:
            _workflowTasks = new Queue<PSTask>();
            _workflowErrors = new List<dynamic>();
            _workflowVars = new Dictionary<string, PSVariable>();

            // Default: the Auto-Restore option is disabled by default:
            _workflowAutoRestore = restore;

            // Default view type:
            _workflowViewType = type;

        }

        public void AddTask(PSTask task)
        {
            // Queue the PSTask for execution:
            _workflowTasks.Enqueue(task);
        }

        public void AddTasks(PSTask[] tasks)
        {
            // Adds each task from 
            foreach (var task in tasks)
            {
                _workflowTasks.Enqueue(task);
            }
        }

        public PSTask RemoveTask()
        {
            // Dequeues and prepares the task for execution:
            return _workflowTasks.Dequeue();
        }

        public void CreateTask(string name, ScriptBlock command, ScriptBlock restore = null)
        {
            // Create the specified task:
            _workflowTasks.Enqueue(new PSTask(name, command, restore));
        }

        public void Execute(PowerShell session)
        {
            // Execute each task for the Tasks queue:
            while (_workflowTasks.Count > 0)
            {
                // Dequeue the task and complete its execution cycle:
                var task = _workflowTasks.Dequeue();

                // Add the task command, parameter(s), etc:
                session.AddScript(task.ScriptCommand.Ast.Extent.Text);

                if (_workflowViewType == TaskType.Input)
                {
                    session.AddParameter("Value", task.Input);
                }

                try
                {

                    // Now that our current task is in the session, invoke:
                    var objReturned = session.Invoke();

                    // Set the output of the task:
                    task.AddOutput(objReturned);

                    // IF autorestore is enabled - true - then restore on errors:
                    if (session.HadErrors)
                    {
                        // Run RestoreScript if it exists:
                        if (_workflowAutoRestore && task.RestoreCommand != null)
                        {
                            // Execute the RestoreCommand script:
                            session.AddScript(task.RestoreCommand.Ast.Extent.Text);

                            // Add restore errors, if any:
                            _workflowErrors.AddRange(session.Invoke());
                        }
                        else
                        {

                        }
                    }

                }
                catch (Exception ex)
                {
                    // Log the full error and object:
                    PSFlowFactory.LogException(ex);
                }

            }
        }

        public void AddVariable(ParameterAst param)
        {
            // Create a PSVariable for the workflow:
            var tempAttributes = new Collection<Attribute>();
            var varName = param.Name.VariablePath.UserPath;
            //tempAttributes.Add(new (param.Attributes[0].TypeName.GetReflectionAttributeType().) );

            var tempVar = new PSVariable(varName, param.StaticType, ScopedItemOptions.Unspecified, tempAttributes);

            // Add the PSVariable to the collection:
            _workflowVars.Add(varName, tempVar);
        }

        public void SetVariable(string name, object value)
        {
            if (_workflowVars[name].IsValidValue(value))
            {
                _workflowVars[name].Value = value;
            }
        }

        public PSVariable GetVariable(string name)
        {
            return _workflowVars[name];
        }

        [DataMember]
        public Dictionary<string, PSVariable> Variables
        {
            get { return _workflowVars; }
            set { _workflowVars = value; }
        }

        [DataMember]
        public Queue<PSTask> Tasks
        {
            get { return _workflowTasks; }
            set { _workflowTasks = value; }
        }

        [DataMember]
        public TaskType TaskType
        {
            get { return _workflowViewType; }
            set { _workflowViewType = value; }
        }

    }
}
