﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Runtime.Serialization;
using Microsoft.PowerShell.EditorServices;
using Newtonsoft.Json;

namespace PSWUI.Shared.WCF.PSFlow
{
    [DataContract]
    public class PSWorkflowModel
    {

        // Declarations:
        private List<PSWorkFlow> _workFlows;
        private PowerShell _psSession;
        private Runspace _psRunspace;
        private ScriptFile _psScriptFile;
        private List<string> _psLogs;
        private int _psCurrentFlow;

        // Methods:

        public PSWorkflowModel() : base()
        {
            _psRunspace = RunspaceFactory.CreateRunspace();
            _psSession = PowerShell.Create();
            _psRunspace.Open();
            _psSession.Runspace = _psRunspace;
            _workFlows = new List<PSWorkFlow>();
            _psCurrentFlow = 0;

        }

        public void AddWorkFlow(PSWorkFlow flow)
        {
            _workFlows.Add(flow);
        }

        public void AddWorkFlowRange(PSWorkFlow[] flows)
        {
            _workFlows.AddRange(flows);
        }

        public void AddScriptFile(ScriptFile script)
        {
            _psScriptFile = script;
        }

        [DataMember]
        public int CurrentIndex
        {
            get { return _psCurrentFlow; }
            set { _psCurrentFlow = value; }
        }

        [DataMember]
        public List<PSWorkFlow> WorkFlows
        {
            get { return _workFlows; }
            set { _workFlows = value; }
        }

        [DataMember]
        [JsonIgnore]
        public PowerShell Session
        {
            get { return _psSession; }
            set { _psSession = value; }
        }

        [DataMember]
        public List<string> Logs
        {
            get { return _psLogs; }
            set { _psLogs = value; }
        }

        [DataMember]
        public PSWorkFlow CurrentFlow
        {
            get { return _workFlows[_psCurrentFlow]; }
        }

        public string ModelName
        {
            get { return _psScriptFile.Id; }
        }

        public void Serialize(string path)
        {
            var js = new JsonSerializer()
            {
                Formatting = Formatting.Indented
            };
            js.Serialize(new StreamWriter(path), this);
        }

    }
}
