﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Windows.Controls;
using PSWUI.Shared.WCF.PSFlow;

namespace PSWUI.Shared.WCF
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class PSService : IPSWCFService
    {
        private PSWorkflowModel _psModel;

        public PSService()
        {
            
        }

        public PSService(string scriptPath)
        {
            Create(scriptPath);
        }

        public void Create(string scriptPath)
        {
            _psModel = PSFlowFactory.CreatePSWorkflowModel(scriptPath);
        }

        public void Execute()
        {
            _psModel.CurrentFlow.Execute(_psModel.Session);
        }

        public List<string> Logs()
        {
            return _psModel.Logs;
        }

        public List<PSWorkFlow> WorkFlows()
        {
            return _psModel.WorkFlows;
        }

        public PSWorkflowModel Model
        {
            get { return _psModel; }
            set { _psModel = value; }
        }
    }
}
