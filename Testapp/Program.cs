﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PSWUI;
using PSWUI.Client;
using Eto;
using Eto.Forms;

using System.IO;
using PSWUI.Client.Models;

namespace Testapp
{
    class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            var path = "";

            if (args != null && args.Length > 0)
            {
                path = args[0];
            }

            new Application(Eto.Platforms.Wpf)
    .Run(new ClientView(path));

        }
    }
}
